<?php

namespace Nitra\AuthBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\Document
 */
class AuthSettings
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $captchaAllUser;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $captchaNewUser;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $captchaAllUserReview;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $captchaNewUserReview;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set captchaAllUser
     *
     * @param boolean $captchaAllUser
     * @return self
     */
    public function setCaptchaAllUser($captchaAllUser)
    {
        $this->captchaAllUser = $captchaAllUser;
        return $this;
    }

    /**
     * Get captchaAllUser
     *
     * @return boolean $captchaAllUser
     */
    public function getCaptchaAllUser()
    {
        return $this->captchaAllUser;
    }

    /**
     * Set captchaNewUser
     *
     * @param boolean $captchaNewUser
     * @return self
     */
    public function setCaptchaNewUser($captchaNewUser)
    {
        $this->captchaNewUser = $captchaNewUser;
        return $this;
    }

    /**
     * Get captchaNewUser
     *
     * @return boolean $captchaNewUser
     */
    public function getCaptchaNewUser()
    {
        return $this->captchaNewUser;
    }

    /**
     * Set captchaAllUserReview
     *
     * @param boolean $captchaAllUserReview
     * @return self
     */
    public function setCaptchaAllUserReview($captchaAllUserReview)
    {
        $this->captchaAllUserReview = $captchaAllUserReview;
        return $this;
    }

    /**
     * Get captchaAllUserReview
     *
     * @return boolean $captchaAllUserReview
     */
    public function getCaptchaAllUserReview()
    {
        return $this->captchaAllUserReview;
    }

    /**
     * Set captchaNewUserReview
     *
     * @param boolean $captchaNewUserReview
     * @return self
     */
    public function setCaptchaNewUserReview($captchaNewUserReview)
    {
        $this->captchaNewUserReview = $captchaNewUserReview;
        return $this;
    }

    /**
     * Get captchaNewUserReview
     *
     * @return boolean $captchaNewUserReview
     */
    public function getCaptchaNewUserReview()
    {
        return $this->captchaNewUserReview;
    }
}