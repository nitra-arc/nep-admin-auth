<?php

namespace Nitra\AuthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsType extends AbstractType
{
    protected $yesNoChoices = array(
        null    => '',
        0       => 'settings.choices.no',
        1       => 'settings.choices.yes',
    );

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('captchaAllUser', 'choice', array(
            'label'     => 'settings.fields.captcha_all.label',
            'help'      => 'settings.fields.captcha_all.help',
            'choices'   => $this->yesNoChoices,
        ));
        $builder->add('captchaNewUser', 'choice', array(
            'label'     => 'settings.fields.captcha_new.label',
            'help'      => 'settings.fields.captcha_new.help',
            'choices'   => $this->yesNoChoices,
        ));
        $builder->add('captchaAllUserReview', 'choice', array(
            'label'     => 'settings.fields.review_captcha_all.label',
            'help'      => 'settings.fields.review_captcha_all.help',
            'choices'   => $this->yesNoChoices,
        ));
        $builder->add('captchaNewUserReview', 'choice', array(
            'label'     => 'settings.fields.review_captcha_new.label',
            'help'      => 'settings.fields.review_captcha_new.help',
            'choices'   => $this->yesNoChoices,
        ));
        $builder->add('submit', 'submit', array(
            'label'     => 'settings.fields.save',
            'attr'      => array(
                'class'     => 'save btn-success',
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraAuthBundle',
            'data_class'            => '\Nitra\AuthBundle\Document\AuthSettings',
        ));
    }

    public function getName()
    {
        return 'auth_settings';
    }
}