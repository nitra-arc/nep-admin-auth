<?php

namespace Nitra\AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Nitra\AuthBundle\Document\AuthSettings;
use Nitra\AuthBundle\Form\Type\SettingsType;

class SettingsController extends Controller
{
    /**
     * @Route("/settings", name="auth_settings")
     * @Template("NitraAuthBundle:AuthSettings:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $Settings   = $this->getSettings();
        $form       = $this->getSettingsForm($Settings);
        $translator = $this->get('translator');
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDocumentManager()->persist($Settings);
            $this->getDocumentManager()->flush($Settings);
            $this->get('session')->getFlashBag()->add('success', $translator->trans("action.object.edit.success", array(), 'Admingenerator'));
        } elseif ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error',   $translator->trans("action.object.edit.error", array(), 'Admingenerator'));
        }

        return array(
            'form'  => $form->createView(),
        );
    }

    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->container->get('doctrine_mongodb.odm.document_manager'); }

    /**
     * @param AuthSettings $Settings
     * @return \Symfony\Component\Form\Form
     */
    protected function getSettingsForm($Settings)
    {
        return $this->createForm(new SettingsType(), $Settings, array(
            'action'    => $this->generateUrl('auth_settings'),
            'attr'      => array(
                'class'     => 'admin_form',
            ),
        ));
    }

    /**
     * @return \Nitra\AuthBundle\Document\AuthSettings
     */
    protected function getSettings()
    {
        return $this->getDocumentManager()->getRepository('NitraAuthBundle:AuthSettings')->findOneBy(array()) ?: new AuthSettings();
    }
}