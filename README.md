AuthBundle
==============

Routing
--------------------

```yaml
# app/config/routing.yml

NitraAuthBundle:
    resource:       "@NitraAuthBundle/Resources/config/routing.yml"
    prefix:         /{_locale}/
    defaults:
        _locale:    ru
    requirements:
        _locale:    en|ru

```
AppKernel
-----------------

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Nitra\AuthBundle\NitraAuthBundle(),
        // ...
    );
}
```

Menu
------------------------

```yaml
# app/config/menu.yml

millwright_menu:
    items: #menu items
        authSettings:
            translateDomain: 'menu'
            route: NitraAuthBundle
    tree: #menu containers
        main: #main container
            type: navigation # menu type id
            children:
                authSettings: ~
```